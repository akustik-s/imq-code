<?php
# вызов в сниппете info_landing_new_v6
# применяется на страницах типа https://wwwtest1.flyuia.com/ua/ru/flights/kiev-to-delhi
// sssssssssssssssssssssss
$res='';  
$id = $modx->resource->id;
$c=$modx->getPlaceholder('countryKey');
$l=$modx->getPlaceholder('langKey');

switch ($l) {
	case 'ru':
		$flight_from = 'Рейсы из ';
		$flight_to = 'Рейсы в ';
		$return = "Обратный рейс ";
		break;
	case 'ua':
		$flight_from = 'Рейси з ';
		$flight_to = 'Рейси в ';
		$return = "Зворотний рейс ";
		break;
	case 'de':
		$flight_from = 'Flüge von ';
		$flight_to = 'Flüge nach ';
		$return = "Der Rückflug ";
		break;
	case 'fr':
		$flight_from = 'Vols de ';
		$flight_to = 'Vols à ';
		$return = "Vol retour ";
		break;
	case 'it':
		$flight_from = 'Voli da ';
		$flight_to = 'Voli per ';
		$return = "Volo di ritorno ";
		break;
	case 'hb':
		$flight_from = 'טיסות מ ';
		$flight_to = 'טיסות ';
		$return = "טיסה חוזרת ";
		break;
    case 'pl':
		$flight_from = 'Loty z ';
		$flight_to = 'Loty do ';
		$return = "Lot powrotny ";
		break;
    case 'lt':
		$flight_from = 'Skrydžiai iš ';
		$flight_to = 'Skrydžiai į ';
		$return = "Atgalinis skrydis ";
		break;
    case 'gr':
		$flight_from = 'Πτήσεις από ';
		$flight_to = 'Αεροπορικά εισιτήρια για ';
		$return = "Πτήση επιστροφής ";
		break;
	case 'se':
	    $flight_to = 'Flights to ';
		$return = "Return flight ";
		$return = "Returresa ";
		break;
    case 'es':
        $flight_to = 'Flights to ';
		$return = "Return flight ";
		$return = "Vuelo de regreso ";
		break;
    case 'cz':
        $flight_to = 'Flights to ';
		$return = "Return flight ";
		$return = "Zpáteční let ";
		break;
	case 'tr':
	    $flight_to = 'Flights to ';
		$return = "Return flight ";
		$return = " dönüş uçuşu";
		break;
	default:
		$flight_from = 'Flights from ';
		$flight_to = 'Flights to ';
		$return = "Return flight ";
		break;
}

$sql = "select res.id, res.uri, res.context_key, flightsFrom.value as flightsFrom, flightsTo.value as flightsTo from {$modx->getTableName('modResource')} as res
left join {$modx->getTableName('modTemplateVarResource')} as flightsFrom ON res.id=flightsFrom.contentid AND flightsFrom.tmplvarid=8
left join {$modx->getTableName('modTemplateVarResource')} as flightsTo ON res.id=flightsTo.contentid AND flightsTo.tmplvarid=9
where published = 1 AND deleted = 0 AND flightsFrom.value = '{$flightsTo}' AND flightsTo.value = '{$flightsFrom}'"; // Реверс направлений
// parent = {$parent} AND 
$result = $modx->query($sql);
if($result)
    $res = $result->fetch(PDO::FETCH_ASSOC);
if(!empty($res)){
    //$url = $modx->makeUrl($res['id'], '', '', 'full');
    $url = '/'.$res['context_key'].'/'.$l.'/'.$res['uri'];
    $flightsFromAirport=$modx->runSnippet('getAirportBySlug',['slug'=>$res['flightsFrom'],'lang'=>$l]);
    $flightsToAirport=$modx->runSnippet('getAirportBySlug',['slug'=>$res['flightsTo'],'lang'=>$l]);
    echo '<div class="like-content"><div class="col md-12 col-sm-12 col-xs-12 mb-mobile_16 mb_48">';
    
    echo "<div style=padding-top:30px;font-size:1.1em;><a href={$url}>{$return} {$flightsFromAirport}-{$flightsToAirport}</a></div>";
    echo '</div></div>';
}

//var_dump($flightsFrom);

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-= FROM =-=-=-=-=-=-=-=-==-=-=-=-=#
echo '<div class="like-content mb_48 mt_48">';
$res='';
$sql = "select res.id, res.uri, res.context_key, flightsFrom.value as flightsFrom, flightsTo.value as flightsTo from {$modx->getTableName('modResource')} as res
left join {$modx->getTableName('modTemplateVarResource')} as flightsFrom ON res.id=flightsFrom.contentid AND flightsFrom.tmplvarid=8
left join {$modx->getTableName('modTemplateVarResource')} as flightsTo ON res.id=flightsTo.contentid AND flightsTo.tmplvarid=9
where published = 1 AND deleted = 0 AND flightsFrom.value = '{$flightsFrom}'";
$result = $modx->query($sql);
if($result)
    $res = $result->fetchAll(PDO::FETCH_ASSOC);
    //var_dump($res);
if(!empty($res)){
    echo '<div class="col md-6 col-sm-6 col-xs-12 mb-mobile_16"><table class="blue-table-v2 narrow full-in-mobile grey-dark-text view-more-1 view-more-items">';
	echo '<thead><tr><th colspan="1" width="100%">'.$flight_from.' '.$modx->runSnippet('getAirportBySlug',['slug'=>$flightsFrom,'lang'=>$l]).'</th></tr></thead><tbody>';
    $counter1=0;
    foreach($res as $v){
        if($v['id'] == $id)	continue;
        //$url = $modx->makeUrl($v['id'], '', '', 'full');
        $url = '/'.$v['context_key'].'/'.$l.'/'.$v['uri'];
        $fromAirport=$modx->runSnippet('getAirportBySlug',['slug'=>$v['flightsFrom'],'lang'=>$l]);
        $toAirport=$modx->runSnippet('getAirportBySlug',['slug'=>$v['flightsTo'],'lang'=>$l]);
        $menutitle="{$fromAirport} - {$toAirport}";
		echo '<tr '.(($counter1<=12)?'class="active"':'').'><td><a href="'.$url.'">'.$menutitle.'</a></td></tr>';
		$counter1++;
    }
	echo '</tbody></table></div>';
}


# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-= TO -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-# 
$res='';
$sql = "select res.id, res.uri, res.context_key, flightsFrom.value as flightsFrom, flightsTo.value as flightsTo from {$modx->getTableName('modResource')} as res
left join {$modx->getTableName('modTemplateVarResource')} as flightsFrom ON res.id=flightsFrom.contentid AND flightsFrom.tmplvarid=8
left join {$modx->getTableName('modTemplateVarResource')} as flightsTo ON res.id=flightsTo.contentid AND flightsTo.tmplvarid=9
where published = 1 AND deleted = 0 AND flightsTo.value = '{$flightsTo}'";
$result = $modx->query($sql);
if($result)
    $res = $result->fetchAll(PDO::FETCH_ASSOC);
if(!empty($res)){
	//$res3=$con->query("SELECT new_resource_id FROM seo_pages_data WHERE cityB = '$from' AND type = '$type' AND deleted = 0 AND new_resource_id<>$id AND for_country='$c'",$conid);
	echo '<div class="col md-6 col-sm-6 col-xs-12 mb-mobile_16"><table class="blue-table-v2 narrow full-in-mobile grey-dark-text view-more-1 view-more-items">';
	echo '<thead><tr><th colspan="1" width="100%">'.$flight_to.' '.$modx->runSnippet('getAirportBySlug',['slug'=>$flightsTo,'lang'=>$l]).'</th></tr></thead><tbody>';
    $counter2=0;
    foreach($res as $v){
        if($v['id'] == $id)	continue;
        //$url = $modx->makeUrl($v['id'], '', '', 'full');
        $url = '/'.$v['context_key'].'/'.$l.'/'.$v['uri'];
        $fromAirport=$modx->runSnippet('getAirportBySlug',['slug'=>$v['flightsFrom'],'lang'=>$l]);
        $toAirport=$modx->runSnippet('getAirportBySlug',['slug'=>$v['flightsTo'],'lang'=>$l]);
        $menutitle="{$fromAirport} - {$toAirport}";
		echo '<tr '.(($counter2<=12)?'class="active"':'').'><td><a href="'.$url.'">'.$menutitle.'</a></td></tr>';
		$counter2++;
    }
	echo '</tbody></table></div>';
}
echo '</div></div>';

if($counter2>12||$counter1>12){
    echo '<div class="like-content hidden">
		<div class="view-more no-padding_bottom" data-text-closed="[[!L?l=`View all`]]" data-text-collapsed="[[!L?l=`Collapse`]]" data-view-more-id="1"><i class="flyicons flyicon-chevron-down-in-circle food__arrow-icon"></i>
			<p>[[!L?l=`View all`]]</p>
		</div>
	</div>';
}
echo '</div>';

//return  "<a href={$url}>{$return} {$flightsFromAirport}-{$flightsToAirport}</a>";
# echo '<div style="text-align:center;padding-top:30px;font-size:30px;">'.$modx->runSnippet('return_flight',['parent'=>$modx->resource->parent,'flightsFrom'=>$from,'flightsTo'=>$to]).'</div>';
